# The Note Manager

---

## What is this?

This application is a fun example of what you can do with AngularJS.  A demo is available on [bryanlor.com](http://bryanlor.com) to play with.  There may also be some blog posts breaking down the components of this application too.

---

## Fun Todos

* Add an API backend
* Add a database like MongoDB or SQLite, instead of using local storage 
* Add unit tests


