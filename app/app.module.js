// app/app.module.js

// The beginning of the Widen Note Manager single-page app.
// Enclose this in a closure.  Note: I'm going to enclose the majority of the code in this app in closures since it makes it more readable and clear.
(function() {
    angular
        // Declare our module dependencies
        .module('noteManager', [
            // Primary or 3rd party modules
            'LocalStorageModule',
            'ngAnimate',
            'ngRoute',
            'ui.bootstrap',
            // Our own modules for this app
            'app.people',
            'app.note'
        ])
        .config(configureRoute);

        // The dependency injections will be coded like below for minification purposes.
        configureRoute.$inject = ['$routeProvider','$locationProvider'];

        // Function for the configurations we need to do for the app which is passed in to the config above.
        function configureRoute($routeProvider, $locationProvider) {
            // Configure the routes for the app so that when the users click on the "links", the users will
            // get take to the appropriate section.
            $routeProvider
                // Route for the list of people which will be the user's home page (default page)
                .when('/', {
                    templateUrl: 'app/people/people.html'
                })

                // Route for the notes page where the user can add their note with their "To" and "From" persons
                .when('/note', {
                    templateUrl: 'app/note/note.html'
                })

                .when('/note-archive', {
                    templateUrl: 'app/note/noteArchive.html'
                })

                // Catch-all route
                .otherwise({ redirectTo: '/' });

            $locationProvider.html5Mode(true);
        }

})();
