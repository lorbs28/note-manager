// app/note/note.service.js

// This is the note service/factory
(function() {
    angular
        .module('app.note')
        .factory('noteService', noteService);

    noteService.$inject = ['localStorageService']

    function noteService(localStorageService) {
        var service = {
            sendNote: sendNote,
            deleteNote: deleteNote,
            getNoteArchive: getNoteArchive
        }

        return service;

        // Function that will send the note.
        function sendNote(note) {
            // This would be where we would put our code for sending the note.

            // Create and save the note to local storage.
            createNote(note);
        }

        // Function that will create and store the note in the local storage.
        function createNote(note) {
            // Get the contact list from the local storage or else instantiate an empty array instead...
            var noteArchive = localStorageService.get('noteArchive') || [];
            // ...assign a number to the new person...
            note.index = getNextNoteIndex();
            note.sendDate = new Date();
            // ...push the contact into the people list...
            noteArchive.push(note);
            // ...then set the people list back into the local storage.
            localStorageService.set('noteArchive', noteArchive);
        }

        // Function to delete the selected note.
        function deleteNote(selectedNote) {

            var noteArchive = localStorageService.get('noteArchive');
            
            // Loop through the list of people to find the correct person to delete.
            for (var i = 0; i < noteArchive.length; i++) {
                // Check to make sure the index values match then "splice" (a.k.a. remove element)
                if (noteArchive[i].index === selectedNote.index) {
                    // The following code will remove one element at the given index of i.
                    noteArchive.splice(i,1);
                    // Save our changes by setting the people list object back into the local storage
                    localStorageService.set('noteArchive', noteArchive);
                    break;
                }
            }

        }

        // Return the array of note objects from local storage or else return an empty array
        function getNoteArchive() {
            return localStorageService.get('noteArchive') || [];
        }

        // Function will get the next note index (unique ID) that will be assigned to
        // a person and return it.
        function getNextNoteIndex() {
            var noteIndexStore = localStorageService.get('noteIndexStore') || [];
            // Set the default index to 1 because a note index store may or may not exist yet.
            var index = 1;
            // Assign the default value to the index object
            var indexObj = {index:index};

            // Check to see if a note index store exists in the local storage.
            // If the store exists then proceed to increment the index,
            if (noteIndexStore.length > 0) {
                // If the index exists then we're going to increment it by one then assign to the index variable
                index = ++noteIndexStore[0].index;
                // Set the index store back in to the local storage
                localStorageService.set('noteIndexStore', noteIndexStore);
            } else {
                noteIndexStore.push(indexObj);
                // Save the index value to the local storage so that we can keep track of it.
                // This will save the index with a value of 1 if the person index store in the
                // local storage is empty.
                localStorageService.set('noteIndexStore', noteIndexStore);
            }

            return index;
        }
    }
})();