// app/note/note.controller.js

// This file contains code for the note controller.
(function() {
    // Declare the note controller
    angular
        .module('app.note')
        .controller('NoteController', NoteController);

    // Inject our dependecies
    NoteController.$inject = ['$scope', 'peopleService', 'noteService'];

    function NoteController($scope, peopleService, noteService) {
        var vm = this;

        vm.peopleList = [];
        vm.noteArchive = [];
        vm.peopleListPredicate = '-lastName';
        vm.noteArchivePredicate = '-sendDate';
        vm.note = {};
        vm.addTo = addTo;
        vm.addFrom = addFrom;
        vm.sendNote = sendNote;
        vm.removeNote = removeNote;

        // Call function to load contact list
        loadPeopleList();
        loadNoteArchive();

        // Function to load the contact list.
        function loadPeopleList() {
            vm.peopleList = peopleService.getPeopleList();
        }

        function loadNoteArchive() {
            vm.noteArchive = noteService.getNoteArchive();
        }
        
        // Function to add the selected person to the "to" field
        function addTo(toPerson) {
            vm.note.toEmail = toPerson.email;
        }

        // Function to add the selected person to the "from" field
        function addFrom(fromPerson) {
            vm.note.fromEmail = fromPerson.email;
        }
        
        // Function to send the note.
        function sendNote() {
            noteService.sendNote(vm.note);
            // Reset the form
            $scope.newNoteForm.$setPristine();
            vm.note = {};
        }

        function removeNote(selectedNote) {
            noteService.deleteNote(selectedNote);
            loadNoteArchive();
        }
    }

})();