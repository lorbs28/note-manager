// app/people/people.service.js

// This is the people service/factory
(function() {
    angular
        .module('app.people')
        .factory('peopleService', peopleService);

    peopleService.$inject = ['$http', '$q', 'localStorageService'];

    function peopleService($http, $q, localStorageService) {
        // All accessible members and callable interface of the people service up top
        var service = {
            createPerson: createPerson,
            updatePerson: updatePerson,
            deletePerson: deletePerson,
            getPeopleList: getPeopleList,
            getGreetingMessage: getGreetingMessage
        }

        return service;

        // Add the contact to the
        function createPerson(person) {
            // Get the contact list from the local storage or else instantiate an empty array instead...
            var peopleList = localStorageService.get('peopleList') || [];
            // ...assign a number to the new person...
            person.index = getNextPersonIndex();
            // ...push the contact into the people list...
            peopleList.push(person);
            // ...then set the people list back into the local storage.
            localStorageService.set('peopleList', peopleList);
        }

        // Function for updating the selected person
        function updatePerson(editedPerson) {
            var peopleList = localStorageService.get('peopleList');
            
            // Loop through the list to find the correct person to update.
            for (var i = 0; i < peopleList.length; i++) {
                // Check the index value that is within each object and make sure
                // that they match.  Then replace the object with the new one.  Then
                // break out of the 
                if (peopleList[i].index === editedPerson.index) {
                    peopleList[i] = editedPerson;
                    // Save our changes by setting the people list object back into the local storage
                    localStorageService.set('peopleList', peopleList);
                    break;
                }
            }
        }

        // Function to delete the selected person.
        function deletePerson(selectedPerson) {
            var peopleList = localStorageService.get('peopleList');
            
            // Loop through the list of people to find the correct person to delete.
            for (var i = 0; i < peopleList.length; i++) {
                // Check to make sure the index values match then "splice" (a.k.a. remove element)
                if (peopleList[i].index === selectedPerson.index) {
                    // The following code will remove one element at the given index of i.
                    peopleList.splice(i,1);
                    // Save our changes by setting the people list object back into the local storage
                    localStorageService.set('peopleList', peopleList);
                    break;
                }
            }

        }

        // Return a list of the saved contacts from the local storage or else return an empty array
        function getPeopleList() {
            return localStorageService.get('peopleList') || [];
        }

        // Function will get the next person index (unique ID) that will be assigned to
        // a person and return it.
        function getNextPersonIndex() {
            var personIndexStore = localStorageService.get('personIndexStore') || [];
            // Set the default index to 1 because a person index store may or may not exist yet.
            var index = 1;
            // Assign the default value to the index object
            var indexObj = {index:index};

            // Check to see if a person index store exists in the local storage.
            // If the store exists then proceed to increment the index,
            if (personIndexStore.length > 0) {
                // If the index exists then we're going to increment it by one then assign to the index variable
                index = ++personIndexStore[0].index;
                // Set the index store back in to the local storage
                localStorageService.set('personIndexStore', personIndexStore);
            } else {
                personIndexStore.push(indexObj);
                // Save the index value to the local storage so that we can keep track of it.
                // This will save the index with a value of 1 if the person index store in the
                // local storage is empty.
                localStorageService.set('personIndexStore', personIndexStore);
            }

            return index;
        }

        // Function to get greeting message from RESTful web service
        function getGreetingMessage() {

            return $http({
                url: 'http://localhost:8080/greeting',
                method: 'GET',
                params: { name : 'Bryan' }
            })
        }
    }
})();
