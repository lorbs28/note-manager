// app/people/people.controller.js

// This file contains code for the people modal controller.
(function() {
    // Delare the people controller
    angular
        .module('app.people')
        .controller('PeopleModalController', PeopleModalController);

    // Inject our dependecies
    // $modalInstance is automatically injected by $modal from the people controller
    PeopleModalController.$inject = ['$scope','$modalInstance', 'selectedPerson'];

    function PeopleModalController($scope, $modalInstance, selectedPerson) {
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.selectedPerson = selectedPerson;

        // Function for the save button on the modal
        function save(editedPerson) {
            // Pass the editedPerson object to the close method of the modal instance which will then
            // be usable back in the people controller
            $modalInstance.close(editedPerson);
        }

        // Function for the cancel button on the modal
        function cancel() {
            $modalInstance.dismiss('cancel');
        }

    }

})();
