// app/people/people.controller.js

// This file contains code for the people controller.
(function() {
    // Delare the people controller
    angular
        .module('app.people')
        .controller('PeopleController', PeopleController);

    // Inject our dependecies
    PeopleController.$inject = ['$scope','$modal','peopleService'];

    function PeopleController($scope, $modal, peopleService) {
        var vm = this;

        vm.peopleList = [];
        vm.person = {};
        vm.predicate = '-lastName';
        vm.addPerson = addPerson;
        vm.removePerson = removePerson;
        vm.openEditForm = openEditForm;

        // RESTful web service test
        vm.greeting = {};

        // Call function to load contact list
        init();

        // Function will runs when the view loads.
        function init() {
            loadPeopleList();
            getGreetingMessage();
        }

        // Function that will add the contact to our contact list for the people page
        function addPerson() {
            peopleService.createPerson(vm.person);
            loadPeopleList();
            // Reset the form
            $scope.newPersonForm.$setPristine();
            vm.person = {};
        }

        // Function to remove the selected person from the list of people.
        function removePerson(selectedPerson) {
            peopleService.deletePerson(selectedPerson);
            loadPeopleList();
        }

        // Function to load the contact list.
        function loadPeopleList() {
            vm.peopleList = peopleService.getPeopleList();
        }

        // Function to open the edit form modal
        function openEditForm(selectedPerson) {
            // Find the template that will be used for the modal and then
            // make sure that the appropriate controller is assigned correctly to the modal.
            var modalInstance = $modal.open({
                templateUrl: '/app/people/editInfoForm.html',
                controller: 'PeopleModalController as vm',
                resolve: {
                    selectedPerson: function() {
                        return selectedPerson;
                    }
                }
            });

            // Refresh the list with the new data.
            modalInstance.result.then(function(data) {
                peopleService.updatePerson(data);
            })
        }

        // Function to help test RESTful web
        function getGreetingMessage() {
            peopleService.getGreetingMessage().then(function(response) {
                vm.greeting = response.data;
            });
        }
        
    }

})();
